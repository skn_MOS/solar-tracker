#include <Servo.h>
#define MUX_A D7
#define MUX_B D6
#define MUX_C D5
#define Enable D8
#define ANALOG_INPUT A0

Servo servohori;
int servoh = 0;
int servohLimitHigh = 120;
int servohLimitLow = 0;

Servo servoverti;
int servov = 0;
int servovLimitHigh = 120;
int servovLimitLow = 0;


void setup ()
{
  pinMode(MUX_A, OUTPUT);
  pinMode(Enable, OUTPUT);
  pinMode(MUX_B, OUTPUT);
  pinMode(MUX_C, OUTPUT);
  
  servohori.attach(1);
  servohori.write(60);
  servoverti.attach(3);
  servoverti.write(60);
  Serial.begin(9600);
  delay(500);

}
void changeMux(int c, int b, int a) {
  digitalWrite(MUX_A, a);
  digitalWrite(MUX_B, b);
  digitalWrite(MUX_C, c);
}
void loop()
{
  digitalWrite(Enable, HIGH);
  
  servoh = servohori.read();
  servov = servoverti.read();
  
  float value;
  
  changeMux(LOW, LOW, LOW);
  value = analogRead(ANALOG_INPUT);
  float topl = value;
  
  changeMux(LOW, LOW, HIGH);
  value = analogRead(ANALOG_INPUT);
  float topr = value;
  
  changeMux(LOW, HIGH, LOW);
  value = analogRead(ANALOG_INPUT);
  float botl = value;
  
  changeMux(LOW, HIGH, HIGH);
  value = analogRead(ANALOG_INPUT);
  float botr = value;

  float avgtop = (topl + topr); 
  float avgbot = (botl + botr); 
  float avgleft = (topl + botl); 
  float avgright = (topr + botr); 

  if (avgtop < avgbot)
  {
    servoverti.write(servov + 1);
    if (servov > servovLimitHigh)
    {
      servov = servovLimitHigh;
    }
    delay(10);
  }
  else if (avgbot < avgtop)
  {
    servoverti.write(servov - 1);
    if (servov < servovLimitLow)
    {
      servov = servovLimitLow;
    }
    delay(10);
  }
  else
  {
    servoverti.write(servov);
  }

  if (avgleft > avgright)
  {
    servohori.write(servoh + 1);
    if (servoh > servohLimitHigh)
    {
      servoh = servohLimitHigh;
    }
    delay(10);
  }
  else if (avgright > avgleft)
  {
    servohori.write(servoh - 1);
    if (servoh < servohLimitLow)
    {
      servoh = servohLimitLow;
    }
    delay(10);
  }
  else
  {
    servohori.write(servoh);
  }
  delay(50);
}
